package main

import "testing"

func Test_turnOn(t *testing.T) {
	var input [1000][1000]bool
	res := turnOn(input, 1, 1, 1, 1)
	assertEqual(t, true, res[1][1])
	assertEqual(t, false, res[0][0])
	assertEqual(t, false, res[0][1])
	assertEqual(t, false, res[1][0])
}

func Test_turnOn2(t *testing.T) {
	var input [1000][1000]bool
	res := turnOn(input, 0, 0, 1, 1)
	assertEqual(t, true, res[1][1])
	assertEqual(t, true, res[0][0])
	assertEqual(t, true, res[0][1])
	assertEqual(t, true, res[1][0])
}

func Test_turnOff(t *testing.T) {
	var input [1000][1000]bool
	input[0][0] = true
	input[1][1] = true
	res := turnOff(input, 1, 1, 1, 1)
	assertEqual(t, false, res[1][1])
	assertEqual(t, true, res[0][0])
	assertEqual(t, false, res[0][1])
	assertEqual(t, false, res[1][0])
}

func Test_turnOff2(t *testing.T) {
	var input [1000][1000]bool
	res := turnOff(input, 0, 0, 1, 1)
	assertEqual(t, false, res[1][1])
	assertEqual(t, false, res[0][0])
	assertEqual(t, false, res[0][1])
	assertEqual(t, false, res[1][0])
}

func Test_toggle(t *testing.T) {
	var input [1000][1000]bool
	input[0][0] = true
	input[1][1] = true
	res := toggle(input, 1, 1, 1, 1)
	assertEqual(t, false, res[1][1])
	assertEqual(t, true, res[0][0])
	assertEqual(t, false, res[0][1])
	assertEqual(t, false, res[1][0])
}

func Test_toggle2(t *testing.T) {
	var input [1000][1000]bool
	input[0][0] = true
	input[1][1] = true
	res := toggle(input, 0, 0, 1, 1)
	assertEqual(t, false, res[1][1])
	assertEqual(t, false, res[0][0])
	assertEqual(t, true, res[0][1])
	assertEqual(t, true, res[1][0])
}

func Test_parseInstruction(t *testing.T) {
	res, startx, starty, endx, endy := parseInstruction("turn on 0,1 through 999,72")
	assertEqual(t, "turn on", res)
	assertEqual(t, 0, startx)
	assertEqual(t, 1, starty)
	assertEqual(t, 999, endx)
	assertEqual(t, 72, endy)
}

func Test_countLit(t *testing.T) {
	var input [1000][1000]bool
	input[0][0] = true
	input[1][1] = true
	res := countLit(input)
	assertEqual(t, 2, res)
}

func Test_countLit2(t *testing.T) {
	var input [1000][1000]bool
	input[0][0] = true
	input[1][1] = true
	input[999][999] = true
	res := countLit(input)
	assertEqual(t, 3, res)
}

func Test_puzzle1(t *testing.T) {
	res := countLitFromFile("input6.txt")
	assertEqual(t, 543903, res)
}

func Test_puzzle2(t *testing.T) {
	res := calcBrightnessFromFile("input6.txt")
	assertEqual(t, 14687245, res)
}
