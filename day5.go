package main

import (
	"bufio"
	"log"
	"os"
)

func contains3Vowels(s string) bool {
	var a, e, i, o, u int
	for _, c := range s {
		if c == 'a' {
			a++
		} else if c == 'e' {
			e++
		} else if c == 'i' {
			i++
		} else if c == 'o' {
			o++
		} else if c == 'u' {
			u++
		}
		if a+e+i+o+u == 3 {
			return true
		}
	}
	return false
}

func contains2LettersInARow(s string) bool {
	var prev rune
	for _, c := range s {
		if c == prev {
			return true
		}
		prev = c
	}
	return false
}

func containsNonOverlappingDuplciatePairs(s string) bool {
	m := make(map[string]int)
	var prevPair string
	for i := 1; i < len(s); i++ {
		pair := s[i-1 : i+1]
		if pair == prevPair {
			// overlapping
			prevPair = ""
			continue
		}
		m[pair] = m[pair] + 1
		if m[pair] == 2 {
			return true
		}
		prevPair = pair
	}
	return false
}

func containsRepeatingLetterWithOneLetterBetween(s string) bool {
	for i := 2; i < len(s); i++ {
		if s[i] == s[i-2] {
			return true
		}
	}
	return false
}

func containsBadPairs(s string) bool {
	var prev rune
	for _, c := range s {
		if (prev == 'a' && c == 'b') || (prev == 'c' && c == 'd') || (prev == 'p' && c == 'q') || (prev == 'x' && c == 'y') {
			return true
		}
		prev = c
	}
	return false
}

func isNice(s string) bool {
	return contains3Vowels(s) && contains2LettersInARow(s) && !containsBadPairs(s)
}

func countNiceStrings(n string) int {
	res := 0
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if isNice(scanner.Text()) {
			res++
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return res
}

func isNice2(s string) bool {
	return containsNonOverlappingDuplciatePairs(s) && containsRepeatingLetterWithOneLetterBetween(s)
}

func countNiceStrings2(n string) int {
	res := 0
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		if isNice2(scanner.Text()) {
			res++
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return res
}
