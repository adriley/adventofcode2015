package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strings"
)

func md5CheckStartWithNZero(s string, n int) bool {
	h := md5.New()
	io.WriteString(h, s)
	hash := fmt.Sprintf("%x", h.Sum(nil))
	return hash[0:n] == strings.Repeat("0", n)
}

func findMd5(s string, n int) string {
	res := s
	for i := 1; !md5CheckStartWithNZero(res, n); i++ {
		res = fmt.Sprintf("%s%d", s, i)
	}
	return res
}
