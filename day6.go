package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

func turnOn(grid [1000][1000]bool, startx, starty, endx, endy int) [1000][1000]bool {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			grid[i][j] = true
		}
	}
	return grid
}

func turnOff(grid [1000][1000]bool, startx, starty, endx, endy int) [1000][1000]bool {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			grid[i][j] = false
		}
	}
	return grid
}

func toggle(grid [1000][1000]bool, startx, starty, endx, endy int) [1000][1000]bool {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			grid[i][j] = !grid[i][j]
		}
	}
	return grid
}

var myExp = regexp.MustCompile(`(.*)\s(\d*),(\d*)\sthrough\s(\d*),(\d*)`)

func parseInstruction(s string) (string, int, int, int, int) {
	match := myExp.FindStringSubmatch(s)
	startx, _ := strconv.Atoi(match[2])
	starty, _ := strconv.Atoi(match[3])
	endx, _ := strconv.Atoi(match[4])
	endy, _ := strconv.Atoi(match[5])
	return match[1], startx, starty, endx, endy
}

func countLit(grid [1000][1000]bool) int {
	res := 0
	for i := 0; i < 1000; i++ {
		for j := 0; j < 1000; j++ {
			if grid[i][j] {
				res++
			}
		}
	}
	return res
}

func processLine(grid [1000][1000]bool, s string) [1000][1000]bool {
	inst, startx, starty, endx, endy := parseInstruction(s)
	if inst == "turn on" {
		grid = turnOn(grid, startx, starty, endx, endy)
	} else if inst == "turn off" {
		grid = turnOff(grid, startx, starty, endx, endy)
	} else if inst == "toggle" {
		grid = toggle(grid, startx, starty, endx, endy)
	}
	return grid
}

func countLitFromFile(n string) int {
	var grid [1000][1000]bool
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		grid = processLine(grid, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return countLit(grid)
}

func turnOn2(grid [1000][1000]int, startx, starty, endx, endy int) [1000][1000]int {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			grid[i][j] = grid[i][j] + 1
		}
	}
	return grid
}

func turnOff2(grid [1000][1000]int, startx, starty, endx, endy int) [1000][1000]int {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			if grid[i][j] > 0 {
				grid[i][j] = grid[i][j] - 1
			}
		}
	}
	return grid
}

func toggle2(grid [1000][1000]int, startx, starty, endx, endy int) [1000][1000]int {
	for i := startx; i <= endx; i++ {
		for j := starty; j <= endy; j++ {
			grid[i][j] = grid[i][j] + 2
		}
	}
	return grid
}

func processLine2(grid [1000][1000]int, s string) [1000][1000]int {
	inst, startx, starty, endx, endy := parseInstruction(s)
	if inst == "turn on" {
		grid = turnOn2(grid, startx, starty, endx, endy)
	} else if inst == "turn off" {
		grid = turnOff2(grid, startx, starty, endx, endy)
	} else if inst == "toggle" {
		grid = toggle2(grid, startx, starty, endx, endy)
	}
	return grid
}

func calcBrightness(grid [1000][1000]int) int {
	res := 0
	for i := 0; i < 1000; i++ {
		for j := 0; j < 1000; j++ {
			res = res + grid[i][j]
		}
	}
	return res
}

func calcBrightnessFromFile(n string) int {
	var grid [1000][1000]int
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		grid = processLine2(grid, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return calcBrightness(grid)
}
