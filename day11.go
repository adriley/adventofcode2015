package main

import "strings"

func DoesNotContainBadChars(s string) bool {
	return !strings.ContainsAny(s, "iol")
}

func Contains3Straight(s string) bool {
	for i := 1; i < len(s)-1; i++ {
		x := s[i-1]
		y := s[i]
		z := s[i+1]
		if y-1 == x && y+1 == z {
			return true
		}
	}
	return false
}

func Contains2NonMatchingPairs(s string) bool {
	set := make(map[byte]bool)
	for i := 0; i < len(s)-1; i++ {
		x := s[i]
		y := s[i+1]
		if x == y {
			set[x] = true
		}
	}
	return len(set) > 1
}

func IsGoodPassword(s string) bool {
	return DoesNotContainBadChars(s) && Contains3Straight(s) && Contains2NonMatchingPairs(s)
}

func Increment(s []byte, n int) {
	if n < 0 {
		return
	}
	s[n] = s[n] + 1
	if s[n] == 'z'+1 {
		s[n] = 'a'
		Increment(s, n-1)
	}
}

func FindNextPassword(s string) string {
	for {
		b := []byte(s)
		Increment(b, len(s)-1)
		s = string(b)
		if IsGoodPassword(s) {
			return s
		}
	}
}
