package main

import "testing"

func Test_parseInputLine(t *testing.T) {
	command, output := parseInputLine("123 -> x")
	assertEqual(t, "123", command)
	assertEqual(t, "x", output)
}

func Test_parseInputLine2(t *testing.T) {
	command, output := parseInputLine("x AND y -> d")
	assertEqual(t, "x AND y", command)
	assertEqual(t, "d", output)
}

func Test_parseInputLine3(t *testing.T) {
	command, output := parseInputLine("jn OR jo -> jp")
	assertEqual(t, "jn OR jo", command)
	assertEqual(t, "jp", output)
}

func Test_parseCommand_Constant(t *testing.T) {
	c := parseCommand("123")
	assertEqual(t, constant, c.comm)
	assertEqual(t, "123", c.inputs[0])
}

func Test_parseCommand_Constant2(t *testing.T) {
	c := parseCommand("72")
	assertEqual(t, constant, c.comm)
	assertEqual(t, "72", c.inputs[0])
}

func Test_parseCommand_Constant_text(t *testing.T) {
	c := parseCommand("x")
	assertEqual(t, constant, c.comm)
	assertEqual(t, "x", c.inputs[0])
}

func Test_parseCommand_AND(t *testing.T) {
	c := parseCommand("x AND y")
	assertEqual(t, and, c.comm)
	assertEqual(t, "x", c.inputs[0])
	assertEqual(t, "y", c.inputs[1])
}

func Test_parseCommand_OR(t *testing.T) {
	c := parseCommand("543 OR c")
	assertEqual(t, or, c.comm)
	assertEqual(t, "543", c.inputs[0])
	assertEqual(t, "c", c.inputs[1])
}

func Test_parseCommand_OR2(t *testing.T) {
	c := parseCommand("jn OR jo")
	assertEqual(t, or, c.comm)
	assertEqual(t, "jn", c.inputs[0])
	assertEqual(t, "jo", c.inputs[1])
}

func Test_parseCommand_LSHIFT(t *testing.T) {
	c := parseCommand("e LSHIFT 54")
	assertEqual(t, lshift, c.comm)
	assertEqual(t, "e", c.inputs[0])
	assertEqual(t, "54", c.inputs[1])
}

func Test_parseCommand_RSHIFT(t *testing.T) {
	c := parseCommand("w RSHIFT x")
	assertEqual(t, rshift, c.comm)
	assertEqual(t, "w", c.inputs[0])
	assertEqual(t, "x", c.inputs[1])
}

func Test_parseCommand_NOT(t *testing.T) {
	c := parseCommand("NOT x")
	assertEqual(t, not, c.comm)
	assertEqual(t, "x", c.inputs[0])
}

func Test_runInstructions_2constants(t *testing.T) {
	o := runInstructions([]string{"123 -> x", "456 -> y"})
	assertEqual(t, uint16(123), o["x"].result)
	assertEqual(t, uint16(456), o["y"].result)
}

func Test_runInstructions_2chainedconstants(t *testing.T) {
	o := runInstructions([]string{"123 -> x", "x -> y"})
	assertEqual(t, uint16(123), o["x"].result)
	assertEqual(t, uint16(123), o["y"].result)
}

func Test_runInstructions_2constants_AND(t *testing.T) {
	o := runInstructions([]string{"123 -> x", "456 -> y", "x AND y -> d"})
	assertEqual(t, uint16(123), o["x"].result)
	assertEqual(t, uint16(456), o["y"].result)
	assertEqual(t, uint16(72), o["d"].result)
}

func Test_instruction_calculate_constant(t *testing.T) {
	i := instruction{comm: constant, inputs: []string{"82"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(82), r)
	assertEqual(t, uint16(82), i.result)
}

func Test_instruction_calculate_and_fromconstants(t *testing.T) {
	i := instruction{comm: and, inputs: []string{"123", "456"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(72), r)
	assertEqual(t, uint16(72), i.result)
}

func Test_instruction_calculate_or_fromconstants(t *testing.T) {
	i := instruction{comm: or, inputs: []string{"123", "456"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(507), r)
	assertEqual(t, uint16(507), i.result)
}

func Test_instruction_calculate_lshift_fromconstants(t *testing.T) {
	i := instruction{comm: lshift, inputs: []string{"123", "2"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(492), r)
	assertEqual(t, uint16(492), i.result)
}

func Test_instruction_calculate_rshift_fromconstants(t *testing.T) {
	i := instruction{comm: rshift, inputs: []string{"456", "2"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(114), r)
	assertEqual(t, uint16(114), i.result)
}

func Test_instruction_calculate_not_fromconstants(t *testing.T) {
	i := instruction{comm: not, inputs: []string{"123"}}
	r := i.calculate(nil)
	assertEqual(t, uint16(65412), r)
	assertEqual(t, uint16(65412), i.result)
}

func Test_instruction_calculate_and_fromconstant_and_other_var(t *testing.T) {
	var m map[string]instruction
	m = make(map[string]instruction)
	m["a"] = instruction{result: 123}
	i := instruction{comm: and, inputs: []string{"a", "456"}}
	r := i.calculate(&m)
	assertEqual(t, uint16(72), r)
	assertEqual(t, uint16(72), i.result)
}

func Test_instruction_calculate_and_fromconstant_and_uncalculated_var(t *testing.T) {
	var m map[string]instruction
	m = make(map[string]instruction)
	m["a"] = instruction{comm: constant, inputs: []string{"123"}}
	i := instruction{comm: and, inputs: []string{"a", "456"}}
	r := i.calculate(&m)
	assertEqual(t, uint16(72), r)
	assertEqual(t, uint16(72), i.result)
}

func Test_runInstructions_example_set(t *testing.T) {
	o := runInstructions([]string{"123 -> x", "456 -> y", "x AND y -> d", "x OR y -> e", "x LSHIFT 2 -> f", "y RSHIFT 2 -> g", "NOT x -> h", "NOT y -> i"})
	assertEqual(t, uint16(72), o["d"].result)
	assertEqual(t, uint16(507), o["e"].result)
	assertEqual(t, uint16(492), o["f"].result)
	assertEqual(t, uint16(114), o["g"].result)
	assertEqual(t, uint16(65412), o["h"].result)
	assertEqual(t, uint16(65079), o["i"].result)
	assertEqual(t, uint16(123), o["x"].result)
	assertEqual(t, uint16(456), o["y"].result)
}

func Test_runInstructions_chained_set(t *testing.T) {
	o := runInstructions([]string{"123 -> xi", "456 -> y", "xi AND y -> aa", "aa -> e"})
	assertEqual(t, uint16(72), o["aa"].result)
	assertEqual(t, uint16(72), o["e"].result)
	assertEqual(t, uint16(123), o["xi"].result)
	assertEqual(t, uint16(456), o["y"].result)
}
func Test_runInstructionsFromFile(t *testing.T) {
	o := runInstructionsFromFile("input7.txt")
	assertEqual(t, uint16(16076), o["a"].result)
}

func Test_day7puzzle2(t *testing.T) {
	o := runInstructionsFromFile("input7.txt")
	b := o["b"]
	b.result = o["a"].result
	for k, v := range o {
		temp := v
		temp.result = 0
		o[k] = temp
	}
	o["b"] = b
	calculateAll(&o)
	assertEqual(t, uint16(2797), o["a"].result)
}
