package main

import "testing"

func Test_lookAndSay_1(t *testing.T) {
	res := lookAndSay("1")
	assertEqual(t, "11", res)
}

func Test_lookAndSay_11(t *testing.T) {
	res := lookAndSay("11")
	assertEqual(t, "21", res)
}

func Test_lookAndSay_21(t *testing.T) {
	res := lookAndSay("21")
	assertEqual(t, "1211", res)
}

func Test_lookAndSayRepeat_3times(t *testing.T) {
	res := lookAndSayRepeat("1", 3)
	assertEqual(t, "1211", res)
}

func Test_lookAndSayRepeat_puzzle1(t *testing.T) {
	res := lookAndSayRepeat("1321131112", 40)
	assertEqual(t, 492982, len(res))
}

func Test_lookAndSayRepeat_puzzle2(t *testing.T) {
	res := lookAndSayRepeat("1321131112", 50)
	assertEqual(t, 6989950, len(res))
}
