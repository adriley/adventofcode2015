package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

// Branch and Bound
// https://www.youtube.com/watch?v=1FEP_sNb62k

const inf = 1<<63 - 1

type matrix struct {
	m    [][]int
	rows int
	cols int
}

type node struct {
	id       int
	m        matrix
	parent   *node
	root     *node
	children []*node
	cost     int
}

func NewMatrix(rows, cols int) *matrix {
	ret := new(matrix)
	ret.rows = rows
	ret.cols = cols
	ret.m = make([][]int, cols)
	for i := 0; i < cols; i++ {
		ret.m[i] = make([]int, rows)
	}
	return ret
}

func CopyMatrix(source *matrix) *matrix {
	dest := new(matrix)
	dest.rows = source.rows
	dest.cols = source.cols
	dest.m = make([][]int, dest.cols)
	for i := 0; i < dest.cols; i++ {
		dest.m[i] = make([]int, dest.rows)
	}
	for r := 0; r < dest.rows; r++ {
		for c := 0; c < dest.cols; c++ {
			dest.m[r][c] = source.m[r][c]
		}
	}
	return dest
}

func CreateRootNode(m matrix) *node {
	ret := new(node)
	ret.m = *CopyMatrix(&m)
	ret.cost = ret.m.Reduce()
	ret.root = ret
	return ret
}

func createChildNode(n *node, dest int) *node {
	source := n.id
	ret := new(node)
	ret.id = dest
	ret.m = *CopyMatrix(&n.m)
	ret.m.setRow(source, inf)
	ret.m.setColumn(dest, inf)
	ret.m.m[dest][n.root.id] = inf
	ret.cost = n.m.m[source][dest] + n.cost + ret.m.Reduce()
	ret.parent = n
	ret.root = n.root
	return ret
}

func (n *node) createChildren() {
	m := n.m
	for i := 0; i < m.cols; i++ {
		if m.m[n.id][i] != inf {
			child := createChildNode(n, i)

			n.children = append(n.children, child)
		}
	}
}

func (m *matrix) rowReduce(r int) int {
	min := inf
	for _, v := range m.m[r] {
		if v < min {
			min = v
		}
	}
	if min == inf {
		return 0
	}
	for i := range m.m[r] {
		if m.m[r][i] != inf {
			m.m[r][i] = m.m[r][i] - min
		}
	}
	return min
}

func (m *matrix) rowReduceAll() int {
	ret := 0
	for i := 0; i < m.rows; i++ {
		ret += m.rowReduce(i)
	}
	return ret
}

func (m *matrix) columnReduce(c int) int {
	min := inf
	for i := 0; i < m.rows; i++ {
		if m.m[i][c] < min {
			min = m.m[i][c]
		}
	}
	if min == inf {
		return 0
	}
	for i := 0; i < m.rows; i++ {
		if m.m[i][c] != inf {
			m.m[i][c] = m.m[i][c] - min
		}
	}
	return min
}

func (m *matrix) columnReduceAll() int {
	ret := 0
	for i := 0; i < m.cols; i++ {
		ret += m.columnReduce(i)
	}
	return ret
}

func (m *matrix) Reduce() int {
	ret := 0
	ret += m.rowReduceAll()
	ret += m.columnReduceAll()
	return ret
}

func (m *matrix) setColumn(col int, value int) {
	for i := 0; i < m.rows; i++ {
		m.m[i][col] = value
	}
}

func (m *matrix) setRow(row int, value int) {
	for i := 0; i < m.cols; i++ {
		m.m[row][i] = value
	}
}

func (m *matrix) setAll(value int) {
	for i := 0; i < m.rows; i++ {
		m.setRow(i, value)
	}
}

func ThrowOutCandidatesOverUpper(candidates []*node, upper int) []*node {
	newCandidates := []*node{}
	for _, c := range candidates {
		if c.cost < upper {
			newCandidates = append(newCandidates, c)
		}
	}
	return newCandidates
}

func FindLowestCostCandidate(candidates []*node) (*node, []*node) {
	newCandidates := []*node{}
	best := candidates[0]
	for i := 1; i < len(candidates); i++ {
		c := candidates[i]
		if c.cost < best.cost {
			newCandidates = append(newCandidates, best)
			best = c
		} else {
			newCandidates = append(newCandidates, c)
		}
	}
	return best, newCandidates
}

func prependInt(x []int, y int) []int {
	x = append(x, 0)
	copy(x[1:], x)
	x[0] = y
	return x
}

func SolveBranchAndBound(m *matrix) ([]int, int) {
	target := CreateRootNode(*m)
	candidates := []*node{}
	solutions := []*node{}
	upper := inf
	for {
		solution, newCands := GetCost(target, candidates)
		if solution.cost < upper {
			upper = solution.cost
		}
		solutions = append(solutions, solution)
		candidates = ThrowOutCandidatesOverUpper(newCands, upper)
		if len(candidates) == 0 {
			break
		}
		target, candidates = FindLowestCostCandidate(candidates)
	}
	res := solutions[0]
	for _, s := range solutions {
		if s.cost < res.cost {
			res = s
		}
	}
	cost := res.cost
	answer := []int{res.id}
	for res.parent != nil {
		res = res.parent
		answer = prependInt(answer, res.id)
	}
	return answer, cost
}

func GetCost(n *node, candidates []*node) (*node, []*node) {
	n.createChildren()
	if len(n.children) == 0 {
		return n, candidates
	}
	best := n.children[0]
	for i := 1; i < len(n.children); i++ {
		c := n.children[i]
		if c.cost < best.cost {
			candidates = append(candidates, best)
			best = c
		} else {
			candidates = append(candidates, c)
		}
	}
	return GetCost(best, candidates)
}

var TravSalesExp = regexp.MustCompile(`(.*)\sto\s(.*)\s=\s(.*)`)

type line struct {
	source string
	dest   string
	cost   int
}

func parseTravSalesLine(s string) line {
	match := TravSalesExp.FindStringSubmatch(s)
	source := match[1]
	dest := match[2]
	cost, _ := strconv.Atoi(match[3])
	return line{source, dest, cost}
}

func ParseInput(n string) *matrix {
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	set := make(map[string]int)
	lines := []line{}
	for scanner.Scan() {
		l := parseTravSalesLine(scanner.Text())
		if _, ok := set[l.source]; !ok {
			set[l.source] = len(set) + 1
		}
		if _, ok := set[l.dest]; !ok {
			set[l.dest] = len(set) + 1
		}
		lines = append(lines, l)
	}
	fmt.Println(set)
	myLength := len(set) + 1
	matrix := NewMatrix(myLength, myLength)

	matrix.setAll(inf)
	matrix.setRow(0, 0)
	matrix.setColumn(0, 0)
	matrix.m[0][0] = inf
	for _, l := range lines {
		sourceid := set[l.source]
		destid := set[l.dest]
		matrix.m[sourceid][destid] = l.cost
		matrix.m[destid][sourceid] = l.cost
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return matrix
}

func SolveBruteForceMaximum(m *matrix) ([]int, int) {
	root := CreateRootNode(*m)
	solutions := []*node{}
	solutions = ExpandAll(root, solutions)
	res := solutions[0]
	for _, s := range solutions {
		if s.cost > res.cost {
			res = s
		}
	}
	cost := res.cost
	answer := []int{res.id}
	for res.parent != nil {
		res = res.parent
		answer = prependInt(answer, res.id)
	}
	return answer, cost
}

func ExpandAll(n *node, solutions []*node) []*node {
	n.createChildren()
	if len(n.children) == 0 {
		solutions = append(solutions, n)
		return solutions
	}
	for _, child := range n.children {
		solutions = ExpandAll(child, solutions)
	}
	return solutions
}
