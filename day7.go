package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var inputParse = regexp.MustCompile(`(.*)\s->\s(.*)`)

func parseInputLine(s string) (string, string) {
	match := inputParse.FindStringSubmatch(s)
	return match[1], match[2]
}

type command int

const (
	constant command = iota + 1
	and
	or
	lshift
	rshift
	not
)

type instruction struct {
	comm   command
	inputs []string
	result uint16
}

func calculateInput(input string, m *map[string]instruction) uint16 {
	i, err := strconv.Atoi(input)
	if err == nil {
		return uint16(i)
	}
	inst := (*m)[input]
	res := inst.calculate(m)
	(*m)[input] = inst
	return res
}

func (i *instruction) calculate(m *map[string]instruction) uint16 {
	if i.result != 0 {
		return i.result
	}
	var a, b uint16
	switch c := i.comm; c {
	case and, or, lshift, rshift:
		b = calculateInput(i.inputs[1], m)
		fallthrough
	case constant, not:
		a = calculateInput(i.inputs[0], m)
	}
	switch c := i.comm; c {
	case constant:
		i.result = a
	case and:
		i.result = a & b
	case or:
		i.result = a | b
	case lshift:
		i.result = a << b
	case rshift:
		i.result = a >> b
	case not:
		i.result = ^a
	}
	return i.result
}

var constantCommandParse = regexp.MustCompile(`^([^\s]+)$`)
var pairCommandParse = regexp.MustCompile(`(.*)\s(.*)\s(.*)`)
var notCommandParse = regexp.MustCompile(`NOT\s(.*)`)

func parseCommand(s string) instruction {
	command := constant
	match := constantCommandParse.FindStringSubmatch(s)
	if match != nil {
		return instruction{comm: command, inputs: []string{match[1]}}
	}
	match = pairCommandParse.FindStringSubmatch(s)
	if match != nil {
		switch c := match[2]; c {
		case "AND":
			command = and
		case "OR":
			command = or
		case "LSHIFT":
			command = lshift
		case "RSHIFT":
			command = rshift
		}
		return instruction{comm: command, inputs: []string{match[1], match[3]}}
	}
	match = notCommandParse.FindStringSubmatch(s)
	if match != nil {
		return instruction{comm: not, inputs: []string{match[1]}}
	}
	return instruction{comm: command, inputs: []string{}}
}

func calculateAll(m *map[string]instruction) {
	for i, v := range *m {
		v2 := v
		v2.calculate(m)
		(*m)[i] = v2
	}
}

func runInstructions(instructs []string) map[string]instruction {
	var res map[string]instruction
	res = make(map[string]instruction)
	for _, v := range instructs {
		c, o := parseInputLine(v)
		res[o] = parseCommand(c)
	}
	for i, v := range res {
		v2 := v
		v2.calculate(&res)
		res[i] = v2
	}
	return res
}

func runInstructionsFromFile(fileName string) map[string]instruction {
	fileBytes, err := ioutil.ReadFile(fileName)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	sliceData := strings.Split(string(fileBytes), "\r\n")
	return runInstructions(sliceData)
}
