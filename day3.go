package main

type point struct {
	x int
	y int
}

func move(c rune, pos point) point {
	if c == '>' {
		pos.x = pos.x + 1
	} else if c == '<' {
		pos.x = pos.x - 1
	} else if c == '^' {
		pos.y = pos.y + 1
	} else if c == 'v' {
		pos.y = pos.y - 1
	}
	return pos
}

func countHouses(s string) int {
	pos := point{0, 0}

	m := make(map[point]int)
	m[pos] = 1

	for _, c := range s {
		pos = move(c, pos)
		m[pos] = m[pos] + 1
	}
	return len(m)
}

func countHousesRobo(s string) int {
	santaPos := point{0, 0}
	roboPos := point{0, 0}

	m := make(map[point]int)
	m[santaPos] = 1
	moveSanta := true

	for _, c := range s {
		if moveSanta {
			santaPos = move(c, santaPos)
			m[santaPos] = m[santaPos] + 1
			moveSanta = false
		} else {
			roboPos = move(c, roboPos)
			m[roboPos] = m[roboPos] + 1
			moveSanta = true
		}
	}
	return len(m)
}
