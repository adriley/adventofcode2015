package main

import (
	"strconv"
	"testing"
)

func Test_unescapeString(t *testing.T) {
	res, _ := strconv.Unquote(`"abc"`)
	assertEqual(t, "abc", res)
}

func Test_unescapeString2(t *testing.T) {
	res, _ := strconv.Unquote(`"aaa\"aaa"`)
	assertEqual(t, `aaa"aaa`, res)
}

func Test_calcRawCodeLengths(t *testing.T) {
	code, raw := calcRawCodeLengths(`"aaa\"aaa"`)
	assertEqual(t, 7, code)
	assertEqual(t, 10, raw)
}

func Test_calcRawEncodedLengths(t *testing.T) {
	encoded, raw := calcRawEncodedLengths(`"aaa\"aaa"`)
	assertEqual(t, 16, encoded)
	assertEqual(t, 10, raw)
}

func Test_calcRawCodeLengthsSumInFile(t *testing.T) {
	res := calcRawCodeLengthsSumInFile("input8.txt")
	assertEqual(t, 1350, res)
}

func Test_calcRawEncodedLengthsSumInFile(t *testing.T) {
	res := calcRawEncodedLengthsSumInFile("input8.txt")
	assertEqual(t, 2085, res)
}
