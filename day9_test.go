package main

import (
	"fmt"
	"testing"
)

func Test_NewMatrix(t *testing.T) {
	myLength := 5
	matrix := NewMatrix(myLength, myLength)
	matrix.m[1][2] = 72
	fmt.Println(matrix.m)
}

func testMatrixSetup() *matrix {
	myLength := 5
	matrix := NewMatrix(myLength, myLength)  //	┌						┐
	matrix.m[0] = []int{inf, 20, 30, 10, 11} //	|	∞	20	30	10	11	|
	matrix.m[1] = []int{15, inf, 16, 4, 2}   //	|	15	∞	16	4	2	|
	matrix.m[2] = []int{3, 5, inf, 2, 4}     //	|	3	5	∞	2	4	|
	matrix.m[3] = []int{19, 6, 18, inf, 3}   //	|	19	6	18	∞	3	|
	matrix.m[4] = []int{16, 4, 7, 16, inf}   //	|	16	4	7	16	∞	|
	return matrix                            //	└						┘
}

func Test_matrix_rowReduce(t *testing.T) {
	matrix := testMatrixSetup()
	res := matrix.rowReduce(0)
	assertEqual(t, 10, res)
	//assertEqual(t, []int{inf, 20, 30, 10, 11}, matrix.m[0])
}

func Test_matrix_rowReduceAll(t *testing.T) {
	matrix := testMatrixSetup()
	res := matrix.rowReduceAll()
	assertEqual(t, 21, res)
	//assertEqual(t, []int{inf, 20, 30, 10, 11}, matrix.m[0])
}

func Test_matrix_columnReduce(t *testing.T) {
	matrix := testMatrixSetup()
	res := matrix.columnReduce(0)
	assertEqual(t, 3, res)
	//assertEqual(t, []int{inf, 20, 30, 10, 11}, matrix.m[0])
}

func Test_matrix_columnReduceAll(t *testing.T) {
	matrix := testMatrixSetup()
	res := matrix.columnReduceAll()
	assertEqual(t, 18, res)
	//assertEqual(t, []int{inf, 20, 30, 10, 11}, matrix.m[0])
}

func Test_matrix_Reduce(t *testing.T) {
	matrix := testMatrixSetup()
	res := matrix.Reduce()
	assertEqual(t, 25, res)
	//assertEqual(t, []int{inf, 20, 30, 10, 11}, matrix.m[0])
}

func Test_node_CreateRoot(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	assertEqual(t, 25, n.cost)
	assertEqual(t, 0, n.id)
	// fmt.Println(matrix.m)
	// fmt.Println(n.m.m)
}

func Test_node_createChildNode(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	cn := createChildNode(n, 1)
	assertEqual(t, 35, cn.cost)
	assertEqual(t, 1, cn.id)
}

func Test_matrix_setColumn(t *testing.T) {
	matrix := testMatrixSetup()
	matrix.setColumn(1, inf)
	fmt.Println(matrix.m)
}

func Test_matrix_setRow(t *testing.T) {
	matrix := testMatrixSetup()
	matrix.setRow(1, inf)
	fmt.Println(matrix.m)
}

func Test_node_createChildren(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	n.createChildren()
	assertEqual(t, 4, len(n.children))
	assertEqual(t, 1, n.children[0].id)
	assertEqual(t, 35, n.children[0].cost)
	assertEqual(t, 2, n.children[1].id)
	assertEqual(t, 53, n.children[1].cost)
	assertEqual(t, 3, n.children[2].id)
	assertEqual(t, 25, n.children[2].cost)
	assertEqual(t, 4, n.children[3].id)
	assertEqual(t, 31, n.children[3].cost)
}

func Test_node_createGrandChildren(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	n.createChildren()
	child := n.children[2]
	child.createChildren()
	assertEqual(t, 3, len(child.children))
	assertEqual(t, 1, child.children[0].id)
	assertEqual(t, 28, child.children[0].cost)
	assertEqual(t, 2, child.children[1].id)
	assertEqual(t, 50, child.children[1].cost)
	assertEqual(t, 4, child.children[2].id)
	assertEqual(t, 36, child.children[2].cost)
}

func Test_node_createGrandGrandChildren(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	n.createChildren()
	child := n.children[2]
	child.createChildren()
	grandchild := child.children[0]
	grandchild.createChildren()
	assertEqual(t, 2, len(grandchild.children))
	assertEqual(t, 2, grandchild.children[0].id)
	assertEqual(t, 52, grandchild.children[0].cost)
	assertEqual(t, 4, grandchild.children[1].id)
	assertEqual(t, 28, grandchild.children[1].cost)
}

func Test_node_createGrandGrandGrandChildren(t *testing.T) {
	matrix := testMatrixSetup()
	n := CreateRootNode(*matrix)
	n.createChildren()
	child := n.children[2]
	child.createChildren()
	grandchild := child.children[0]
	grandchild.createChildren()
	grandgrandchild := grandchild.children[1]
	grandgrandchild.createChildren()
	assertEqual(t, 1, len(grandgrandchild.children))
	assertEqual(t, 2, grandgrandchild.children[0].id)
	assertEqual(t, 28, grandgrandchild.children[0].cost)
}

func Test_Solve_OneNode(t *testing.T) {
	matrix := NewMatrix(1, 1)
	matrix.m[0] = []int{inf}
	res, _ := SolveBranchAndBound(matrix)
	assertEqual(t, 0, res[0])
}

func Test_Solve_TwoNode(t *testing.T) {
	matrix := NewMatrix(2, 2)    //	┌			┐
	matrix.m[0] = []int{inf, 20} //	|	∞	20	|
	matrix.m[1] = []int{15, inf} //	|	15	∞	|
	//								└			┘
	res, _ := SolveBranchAndBound(matrix)
	assertEqual(t, 0, res[0])
	assertEqual(t, 1, res[1])
}

func Test_Solve_FiveNodes(t *testing.T) {
	matrix := testMatrixSetup()
	res, cost := SolveBranchAndBound(matrix)
	assertEqual(t, 0, res[0])
	assertEqual(t, 3, res[1])
	assertEqual(t, 1, res[2])
	assertEqual(t, 4, res[3])
	assertEqual(t, 2, res[4])
	assertEqual(t, 28, cost)
}

func Test_ParseInput(t *testing.T) {
	matrix := ParseInput("input9example.txt")
	fmt.Println(matrix.m)
	route, cost := SolveBranchAndBound(matrix)
	fmt.Println(route)
	fmt.Println(cost)
}

func Test_parseTravSalesLine(t *testing.T) {
	line := parseTravSalesLine("London to Dublin = 464")
	assertEqual(t, "London", line.source)
	assertEqual(t, "Dublin", line.dest)
	assertEqual(t, 464, line.cost)
}

func Test_TravellingSalesman_Part1(t *testing.T) {
	matrix := ParseInput("input9.txt")
	fmt.Println(matrix.m)
	route, cost := SolveBranchAndBound(matrix)
	fmt.Println(route)
	fmt.Println(cost)
}

func Test_TravellingSalesman_Part2_example(t *testing.T) {
	matrix := ParseInput("input9example.txt")
	fmt.Println(matrix.m)
	route, cost := SolveBruteForceMaximum(matrix)
	fmt.Println(route)
	fmt.Println(cost)
}

func Test_TravellingSalesman_Part2(t *testing.T) {
	matrix := ParseInput("input9.txt")
	fmt.Println(matrix.m)
	route, cost := SolveBruteForceMaximum(matrix)
	fmt.Println(route)
	fmt.Println(cost)
}
