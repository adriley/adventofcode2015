package main

import (
	"testing"
)

func Test_calcWrapping(t *testing.T) {
	res := calcWrapping(2, 3, 4)
	assertEqual(t, 58, res)
}

func Test_calcWrapping2(t *testing.T) {
	res := calcWrapping(1, 1, 10)
	assertEqual(t, 43, res)
}

func Test_parseLine(t *testing.T) {
	l, w, h := parseLine("3x11x24")
	assertEqual(t, 3, l)
	assertEqual(t, 11, w)
	assertEqual(t, 24, h)
}

func Test_calcRibbon(t *testing.T) {
	res := calcRibbon(2, 3, 4)
	assertEqual(t, 10, res)
}

func Test_calcRibbon2(t *testing.T) {
	res := calcRibbon(1, 1, 10)
	assertEqual(t, 4, res)
}

func Test_calcBow(t *testing.T) {
	res := calcBow(2, 3, 4)
	assertEqual(t, 24, res)
}

func Test_calcBow2(t *testing.T) {
	res := calcBow(1, 1, 10)
	assertEqual(t, 10, res)
}
func Test_calcTotal(t *testing.T) {
	wrapping, ribbon := calcTotal("input2.txt")
	assertEqual(t, 1588178, wrapping)
	assertEqual(t, 3783758, ribbon)
}
