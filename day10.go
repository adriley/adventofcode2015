package main

import (
	"strconv"
	"strings"
)

func lookAndSay(s string) string {
	var res strings.Builder
	char := 'X'
	count := 0
	for _, r := range s {
		if r != char {
			if char != 'X' {
				res.WriteString(strconv.Itoa(count))
				res.WriteRune(char)
			}
			char = r
			count = 0
		}
		count++
	}
	if char != 'X' {
		res.WriteString(strconv.Itoa(count))
		res.WriteRune(char)
	}
	return res.String()
}

func lookAndSayRepeat(s string, r int) string {
	res := s
	for i := 0; i < r; i++ {
		res = lookAndSay(res)
	}
	return res
}
