package main

import "testing"

func Test_DoesNotContainBadChars_true(t *testing.T) {
	res := DoesNotContainBadChars("abc")
	assertEqual(t, true, res)
}

func Test_DoesNotContainBadChars_i(t *testing.T) {
	res := DoesNotContainBadChars("aibc")
	assertEqual(t, false, res)
}

func Test_Contains3Straight_false(t *testing.T) {
	res := Contains3Straight("amz")
	assertEqual(t, false, res)
}

func Test_Contains3Straight_true(t *testing.T) {
	res := Contains3Straight("abccnm")
	assertEqual(t, true, res)
}

func Test_Contains2NonMatchingPairs_false(t *testing.T) {
	res := Contains2NonMatchingPairs("aaaaaa")
	assertEqual(t, false, res)
}

func Test_Contains2NonMatchingPairs_true(t *testing.T) {
	res := Contains2NonMatchingPairs("aaaabb")
	assertEqual(t, true, res)
}

func Test_IsGoodPassword_false(t *testing.T) {
	res := IsGoodPassword("hijklmmn")
	assertEqual(t, false, res)
}

func Test_IsGoodPassword_false2(t *testing.T) {
	res := IsGoodPassword("abbceffg")
	assertEqual(t, false, res)
}

func Test_IsGoodPassword_false3(t *testing.T) {
	res := IsGoodPassword("abbcegjk")
	assertEqual(t, false, res)
}

func Test_FindNextPassword(t *testing.T) {
	res := FindNextPassword("abcdefgh")
	assertEqual(t, "abcdffaa", res)
}

func Test_FindNextPassword2(t *testing.T) {
	res := FindNextPassword("ghijklmn")
	assertEqual(t, "ghjaabcc", res)
}

func Test_FindNextPassword_Answer1(t *testing.T) {
	res := FindNextPassword("hepxcrrq")
	assertEqual(t, "hepxxyzz", res)
}

func Test_FindNextPassword_Answer2(t *testing.T) {
	res := FindNextPassword("hepxxyzz")
	assertEqual(t, "heqaabcc", res)
}
