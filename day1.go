package main

func resolveFloor(s string) (int, int) {
	res := 0
	pos := 0
	for i, c := range s {
		if c == '(' {
			res++
		} else if c == ')' {
			res--
			if res < 0 && pos == 0 {
				pos = i + 1
			}
		}
	}
	return res, pos
}
