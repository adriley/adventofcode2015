package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func min(x, y, z int) int {
	if x < y && x < z {
		return x
	} else if y < z {
		return y
	} else {
		return z
	}
}

func min2(x, y, z int) (int, int) {
	if x > y && x > z {
		return y, z
	} else if y > z {
		return x, z
	} else {
		return x, y
	}
}

func calcWrapping(l, w, h int) int {
	s1 := l * w
	s2 := w * h
	s3 := h * l
	extra := min(s1, s2, s3)
	return (2 * s1) + (2 * s2) + (2 * s3) + extra
}

func calcRibbon(l, w, h int) int {
	a, b := min2(l, w, h)
	return (2 * a) + (2 * b)
}

func calcBow(l, w, h int) int {
	return l * w * h
}

func parseLine(l string) (int, int, int) {
	s := strings.Split(l, "x")
	a := make([]int, len(s))
	for i := range a {
		a[i], _ = strconv.Atoi(s[i])
	}
	return a[0], a[1], a[2]
}

func calcTotal(n string) (int, int) {
	wrapping := 0
	ribbon := 0
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		l, w, h := parseLine(scanner.Text())
		wrapping += calcWrapping(l, w, h)
		ribbon += calcRibbon(l, w, h) + calcBow(l, w, h)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return wrapping, ribbon
}
