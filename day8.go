package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func calcRawCodeLengths(s string) (int, int) {
	res, _ := strconv.Unquote(s)
	return len(res), len(s)
}

func calcRawEncodedLengths(s string) (int, int) {
	res := strconv.Quote(s)
	return len(res), len(s)
}

func calcRawCodeLengthsSumInFile(n string) int {
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	res := 0
	for scanner.Scan() {
		code, raw := calcRawCodeLengths(scanner.Text())
		res = res + raw - code
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return res
}

func calcRawEncodedLengthsSumInFile(n string) int {
	f, err := os.Open(n)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	res := 0
	for scanner.Scan() {
		encoded, raw := calcRawEncodedLengths(scanner.Text())
		res = res + encoded - raw
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return res
}
