package main

import "testing"

func Test_contains3Vowels_simple_true(t *testing.T) {
	res := contains3Vowels("aei")
	assertEqual(t, true, res)
}

func Test_contains3Vowels_simple_true2(t *testing.T) {
	res := contains3Vowels("uio")
	assertEqual(t, true, res)
}

func Test_contains3Vowels_simple_false(t *testing.T) {
	res := contains3Vowels("qwpmdwa")
	assertEqual(t, false, res)
}

func Test_contains1RepeatedVowel_false(t *testing.T) {
	res := contains3Vowels("aaa")
	assertEqual(t, true, res)
}

func Test_contains2LettersInARow_simple_true(t *testing.T) {
	res := contains2LettersInARow("abcdde")
	assertEqual(t, true, res)
}

func Test_contains2LettersInARow_simple_false(t *testing.T) {
	res := contains2LettersInARow("uidsfdsfcso")
	assertEqual(t, false, res)
}

func Test_containsBadPairs_ab_true(t *testing.T) {
	res := containsBadPairs("abcdde")
	assertEqual(t, true, res)
}

func Test_containsBadPairs_cd_true(t *testing.T) {
	res := containsBadPairs("dfdscdde")
	assertEqual(t, true, res)
}

func Test_containsBadPairs_pq_true(t *testing.T) {
	res := containsBadPairs("dpqde")
	assertEqual(t, true, res)
}

func Test_containsBadPairs_xy_true(t *testing.T) {
	res := containsBadPairs("dsdsfvxy")
	assertEqual(t, true, res)
}

func Test_containsBadPairs_simple_false(t *testing.T) {
	res := containsBadPairs("uidsfdsfcso")
	assertEqual(t, false, res)
}

func Test_isNice_1(t *testing.T) {
	res := isNice("ugknbfddgicrmopn")
	assertEqual(t, true, res)
}

func Test_isNice_2(t *testing.T) {
	res := isNice("aaa")
	assertEqual(t, true, res)
}

func Test_isNice_3(t *testing.T) {
	res := isNice("jchzalrnumimnmhp")
	assertEqual(t, false, res)
}

func Test_isNice_4(t *testing.T) {
	res := isNice("haegwjzuvuyypxyu")
	assertEqual(t, false, res)
}

func Test_isNice_5(t *testing.T) {
	res := isNice("dvszwmarrgswjxmb")
	assertEqual(t, false, res)
}

func Test_countNiceStrings(t *testing.T) {
	res := countNiceStrings("input5.txt")
	assertEqual(t, 238, res)
}

func Test_containsNonOverlappingDuplciatePairs_true(t *testing.T) {
	res := containsNonOverlappingDuplciatePairs("xyxy")
	assertEqual(t, true, res)
}

func Test_containsNonOverlappingDuplciatePairs_true2(t *testing.T) {
	res := containsNonOverlappingDuplciatePairs("aabcdefgaa")
	assertEqual(t, true, res)
}

func Test_containsNonOverlappingDuplciatePairs_false(t *testing.T) {
	res := containsNonOverlappingDuplciatePairs("ieodomkazucvgmuy")
	assertEqual(t, false, res)
}

func Test_containsNonOverlappingDuplciatePairs_overlapping_false(t *testing.T) {
	res := containsNonOverlappingDuplciatePairs("aaa")
	assertEqual(t, false, res)
}

func Test_containsNonOverlappingDuplciatePairs_nonoverlapping_adjacent_true(t *testing.T) {
	res := containsNonOverlappingDuplciatePairs("aaaa")
	assertEqual(t, true, res)
}

func Test_containsRepeatingLetterWithOneLetterBetween_true(t *testing.T) {
	res := containsRepeatingLetterWithOneLetterBetween("aaaa")
	assertEqual(t, true, res)
}

func Test_containsRepeatingLetterWithOneLetterBetween_true2(t *testing.T) {
	res := containsRepeatingLetterWithOneLetterBetween("xyx")
	assertEqual(t, true, res)
}

func Test_containsRepeatingLetterWithOneLetterBetween_true3(t *testing.T) {
	res := containsRepeatingLetterWithOneLetterBetween("abcdefeghi")
	assertEqual(t, true, res)
}

func Test_isNice2_1(t *testing.T) {
	res := isNice2("qjhvhtzxzqqjkmpb")
	assertEqual(t, true, res)
}

func Test_isNice2_2(t *testing.T) {
	res := isNice2("xxyxx")
	assertEqual(t, true, res)
}

func Test_isNice2_3(t *testing.T) {
	res := isNice2("uurcxstgmygtbstg")
	assertEqual(t, false, res)
}

func Test_isNice2_4(t *testing.T) {
	res := isNice2("ieodomkazucvgmuy")
	assertEqual(t, false, res)
}

func Test_countNiceStrings2(t *testing.T) {
	res := countNiceStrings2("input5.txt")
	assertEqual(t, 69, res)
}
