package main

import "testing"

func Test_md5Check_true(t *testing.T) {
	res := md5CheckStartWithNZero("abcdef609043", 5)
	assertEqual(t, true, res)
}

func Test_md5Check_false(t *testing.T) {
	res := md5CheckStartWithNZero("abcdef000000", 5)
	assertEqual(t, false, res)
}

func Test_findMd5(t *testing.T) {
	res := findMd5("abcdef", 5)
	assertEqual(t, "abcdef609043", res)
}

func Test_findMd5_secondExample(t *testing.T) {
	res := findMd5("pqrstuv", 5)
	assertEqual(t, "pqrstuv1048970", res)
}

func Test_findMd5_puzzleAnswer(t *testing.T) {
	res := findMd5("ckczppom", 5)
	assertEqual(t, "ckczppom117946", res)
}

func Test_findMd5_puzzleAnswer2(t *testing.T) {
	res := findMd5("ckczppom", 6)
	assertEqual(t, "ckczppom3938038", res)
}
